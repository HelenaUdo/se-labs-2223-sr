import csv

file = open('ex2-text.csv', mode = 'r')
csvreader = csv.reader(file, delimiter=',')

header = []
header = next(csvreader)
print(header)

rows = []
for row in csvreader:
        rows.append(row)

file.close()

wfile = open('ex2-employees.txt', mode = 'w')
csvwriter = csv.writer(wfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL )
csvwriter.writerow(['John Smith', 'Accounting'])
wfile.close()

wfile = open('ex2-locations.txt', mode = 'w')
csvwriter = csv.writer(wfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL )
csvwriter.writerow(['John Smith', '13-05'])
wfile.close()